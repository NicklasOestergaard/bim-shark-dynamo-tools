﻿using Autodesk.DesignScript.Runtime;
using BIMSharkDynamoToolsZeroTouch.Utils;
using CoreNodeModels;
using Dynamo.Graph.Nodes;
using Newtonsoft.Json;
using ProtoCore.AST.AssociativeAST;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BIMSharkDynamoToolsUI.Utilities
{
    /*
    * This exmple shows how to create a UI node for Dynamo
    * which loads custom data-bound UI into the node's view
    * at run time.

    * Dynamo uses the MVVM model of programming,
    * in which the UI is data-bound to the view model, which
    * exposes data from the underlying model. Custom UI nodes
    * are a hybrid because NodeModel objects already have an
    * associated NodeViewModel which you should never need to
    * edit. So here we will create a data binding between
    * properties on our class and our custom UI.
    */

    // The NodeName attribute is what will display on
    // top of the node in Dynamo
    /// <summary>
    /// fgf
    /// </summary>
    [NodeName("DropDownFromDictionary<Key, Value>")]
    // The NodeCategory attribute determines how your
    // node will be organized in the library. You can
    // specify your own category or by default the class
    // structure will be used.  You can no longer
    // add packages or custom nodes to one of the
    // built-in OOTB library categories.
    [NodeCategory("BIMSharkDynamoTools.Utilities")]
    // The description will display in the tooltip
    // and in the help window for the node.
    [NodeDescription("Creat a dropdown from Dictionary<Key, Value>")]
    // Specifying InPort and OutPort types simply
    // adds these types to the help window for the
    // node when hovering the name in the library.
    [InPortTypes("List<string>", "List<object>")]
    [OutPortTypes("Dictionary<Key, Value>")]
    // Add the IsDesignScriptCompatible attribute to ensure
    // that it gets loaded in Dynamo.
    [IsDesignScriptCompatible]
    public class DropDownFromDictionary : DSDropDownBase
    {
        #region Properties
        /// <summary>
        /// Bar chart labels.
        /// </summary>
        public List<string> Keys { get; set; }

        /// <summary>
        /// Bar chart values.
        /// </summary>
        public List<object> Values { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Instantiate a new NodeModel instance.
        /// </summary>
        public DropDownFromDictionary()
        {
            // When you create a UI node, you need to do the
            // work of setting up the ports yourself. To do this,
            // you can populate the InPorts and the OutPorts
            // collections with PortData objects describing your ports.
            InPorts.Add(new PortModel(PortType.Input, this, new PortData("Dictionary.Keys", "a list of keys (stings)")));
            InPorts.Add(new PortModel(PortType.Input, this, new PortData("Dictionary.Values", "a list of values (Object)")));

            // Nodes can have an arbitrary number of inputs and outputs.
            // If you want more ports, just create more PortData objects.
            OutPorts.Add(new PortModel(PortType.Output, this, new PortData("Dictionary", "Dictionary containing label:value key-pairs")));
            //OutPorts.Add(new PortModel(PortType.Output, this, new PortData("window value", "returns the string value displayed in our window when button is pressed")));

            // This call is required to ensure that your ports are
            // properly created.
            RegisterAllPorts();

            // Listen for input port disconnection to trigger button UI update
            PortDisconnected += DropDownCustomNodeModel_PortDisconnected;

            ArgumentLacing = LacingStrategy.Disabled;
        }

        // Starting with Dynamo v2.0 you must add Json constructors for all nodeModel
        // dervived nodes to support the move from an Xml to Json file format.  Failing to
        // do so will result in incorrect ports being generated upon serialization/deserialization.
        // This constructor is called when opening a Json graph. We must also pass the deserialized
        // ports with the json constructor and then call the base class passing the ports as parameters.
        /// <summary>
        /// fgf
        /// </summary>
        /// <param name="inPorts"></param>
        /// <param name="outPorts"></param>
        [JsonConstructor]

        public DropDownFromDictionary(IEnumerable<PortModel> inPorts, IEnumerable<PortModel> outPorts) : base("item", inPorts, outPorts)
        {
            PortDisconnected += DropDownCustomNodeModel_PortDisconnected;
        }
        #endregion

        #region Events
        private void DropDownCustomNodeModel_PortDisconnected(PortModel port)
        {
            // Clear UI when a input port is disconnected
            if (port.PortType == PortType.Input && this.State == ElementState.Active)
            {
                Keys.Clear();
                Values.Clear();

                RaisePropertyChanged("DataUpdated");
            }
        }
        #endregion

        #region Databridge
        // Use the VMDataBridge to safely retrieve our input values
        /// <summary>
        /// Register the data bridge callback.
        /// </summary>
        protected override void OnBuilt()
        {
            base.OnBuilt();
            VMDataBridge.DataBridge.Instance.RegisterCallback(GUID.ToString(), DataBridgeCallback);
        }

        /// <summary>
        /// Unregister the data bridge callback.
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            VMDataBridge.DataBridge.Instance.UnregisterCallback(GUID.ToString());
        }

        /// <summary>
        /// Callback method for DataBridge mechanism.
        /// This callback only gets called when
        ///     - The AST is executed
        ///     - After the BuildOutputAST function is executed
        ///     - The AST is fully built
        /// </summary>
        /// <param name="data">The data passed through the data bridge.</param>
        private void DataBridgeCallback(object data)
        {
            // Grab input data which always returned as an ArrayList
            ArrayList inputs = data as ArrayList;

            if (inputs.Count != 2)
            {
                throw new Exception("Please connect a list to all inputs");
            }

            if (inputs[0].GetType() != typeof(ArrayList) || inputs[1].GetType() != typeof(ArrayList))
            {
                throw new Exception("Keys and Values do not properly align in length.");
            }

            ArrayList keys = (ArrayList)inputs[0];
            ArrayList values = (ArrayList)inputs[1];

            // Only continue if key/values match in length
            if (keys?.Count != values?.Count || keys?.Count < 1)
            {
                throw new Exception("Keys and Values do not properly align in length.");
            }

            // Update properties

            // The Items collection contains the elements
            // that appear in the list. For this example, we
            // clear the list before adding new items, but you
            // can also use the PopulateItems method to add items
            // to the list.
            Items.Clear();

            Keys = new List<string>();
            Values = new List<object>();
            // Create a number of DynamoDropDownItem objects
            // to store the items that we want to appear in our list.
            for (var i = 0; i < keys?.Count; i++)
            {
                Keys.Add(item: (string)keys[i]);
                Values.Add(values[i]);
                Items.Add(new DynamoDropDownItem((string)keys[i], (string)values[i]));
            }

            //var newItems = new List<DynamoDropDownItem>()
            //{
            //    new DynamoDropDownItem("NVO Tywin", 0),
            //    new DynamoDropDownItem("NVO Cersei", 1),
            //    new DynamoDropDownItem("NVO Hodor",2),
            //    new DynamoDropDownItem(keys[0] + " ; "+  values[0], Values[0]),
            //    new DynamoDropDownItem(keys[1] + " ; "+  values[1], Values[1])
            //};

            //Items.AddRange(newItems);

            // Set the selected index to something other
            // than -1, the default, so that your list
            // has a pre-selection.

            SelectedIndex = 0;

            // Notify UI the data has been modified
            RaisePropertyChanged("DataUpdated");
        }

        /// <summary>
        /// PopulateItemsCore
        /// </summary>
        /// <param name="currentSelection"></param>
        /// <returns></returns>
        protected override SelectionState PopulateItemsCore(string currentSelection)
        {
            //// The Items collection contains the elements
            //// that appear in the list. For this example, we
            //// clear the list before adding new items, but you
            //// can also use the PopulateItems method to add items
            //// to the list.

            //Items.Clear();

            //// Create a number of DynamoDropDownItem objects
            //// to store the items that we want to appear in our list.

            //var newItems = new List<DynamoDropDownItem>()
            //{
            //    new DynamoDropDownItem("Tywin", 0),
            //    new DynamoDropDownItem("Cersei", 1),
            //    new DynamoDropDownItem("Hodor",2)
            //};

            //Items.AddRange(newItems);

            //// Set the selected index to something other
            //// than -1, the default, so that your list
            //// has a pre-selection.

            RaisePropertyChanged("DataUpdated");

            if (string.IsNullOrEmpty(currentSelection))
            {
                SelectedIndex = 0;
            }
            return SelectionState.Restore;
        }
        #endregion

        #region Methods
        /// <summary>
        /// BuildOutputAst is where the outputs of this node are calculated.
        /// This method is used to do the work that a compiler usually does
        /// by parsing the inputs List inputAstNodes into an abstract syntax tree.
        /// </summary>
        /// <param name="inputAstNodes"></param>
        [IsVisibleInDynamoLibrary(false)]
        public override IEnumerable<AssociativeNode> BuildOutputAst(List<AssociativeNode> inputAstNodes)
        {
            // WARNING!!!
            // Do not throw an exception during AST creation.

            // If inputs are not connected return null
            if (!InPorts[0].Connectors.Any() ||
                !InPorts[1].Connectors.Any())
            {
                return new[]
                {
                    AstFactory.BuildAssignment(GetAstIdentifierForOutputIndex(0), AstFactory.BuildNullNode()),
                };
                //throw new ArgumentException("Please connect list to all inputs");
            }

            if (SelectedIndex >= 0)
            {
                //var MyOutput = (int)Items[SelectedIndex].Item;
            }

            AssociativeNode inputNode = AstFactory.BuildFunctionCall(
                new Func<List<string>, List<object>, int, Dictionary<string, object>>(DropDownFunctions.GetNodeInput),
                //new List<AssociativeNode> { inputAstNodes[0], inputAstNodes[1], AstFactory.BuildIntNode(SelectedIndex) }
                new List<AssociativeNode> { inputAstNodes[0], inputAstNodes[1], AstFactory.BuildIntNode(SelectedIndex) }

            );

            return new[]
            {
                AstFactory.BuildAssignment(GetAstIdentifierForOutputIndex(0), inputNode),
                    AstFactory.BuildAssignment(
                        AstFactory.BuildIdentifier(AstIdentifierBase + "_dummy"),
                        VMDataBridge.DataBridge.GenerateBridgeDataAst(GUID.ToString(), AstFactory.BuildExprList(inputAstNodes)
                    )
                )
            };
        }
        #endregion
    }
}

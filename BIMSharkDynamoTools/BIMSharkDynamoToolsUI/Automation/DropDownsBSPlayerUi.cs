﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using CoreNodeModels;
using DSRevitNodesUI;
using Dynamo.Graph.Nodes;
using Dynamo.Utilities;
using Newtonsoft.Json;
using ProtoCore.AST.AssociativeAST;
using RevitServices.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using ElementSelector = Revit.Elements.ElementSelector;

namespace BIMSharkDynamoToolsUI.Automation
{
    /// <summary>
    /// dfgdg
    /// </summary>
    [NodeName("BSSelectedElements")]
    [NodeCategory("BIMSharkDynamoTools.Revit")]
    [NodeDescription("Retrieve Selected Elements.")]
    [IsDesignScriptCompatible]
    public class BSPlayerUi : RevitDropDownBase
    {
        /// <summary>
        /// hjhk
        /// </summary>
        /// <param name="inPorts"></param>
        /// <param name="outPorts"></param>
        [JsonConstructor]
        public BSPlayerUi(IEnumerable<PortModel> inPorts, IEnumerable<PortModel> outPorts) : base("SelectedElement", inPorts, outPorts) { }

        /// <summary>
        /// hjhk
        /// </summary>
        public BSPlayerUi() : base("SelectedElement") { }

        /// <summary>
        /// dfgfdg
        /// </summary>
        /// <param name="currentSelection"></param>
        protected override SelectionState PopulateItemsCore(string currentSelection)
        {
            Items.Clear();

            UIApplication uiapp = DocumentManager.Instance.CurrentUIApplication;
            UIDocument uiDoc = uiapp.ActiveUIDocument;
            Document doc = DocumentManager.Instance.CurrentDBDocument;
            ICollection<ElementId> CurrentSelectedIds = uiDoc.Selection.GetElementIds();

            if (CurrentSelectedIds.Count == 0)
            {
                Items.Add(new DynamoDropDownItem("No selected element found.", null));
                SelectedIndex = 0;
                return SelectionState.Done;
            }

            foreach (ElementId ele in CurrentSelectedIds)
            {
                Element element = doc.GetElement(ele);
                Items.Add(new DynamoDropDownItem(element.Name, element));
            }

            Items = Items.OrderBy(x => x.Name).ToObservableCollection();
            return SelectionState.Restore;
        }

        /// <summary>
        /// dfgdg
        /// </summary>
        /// <param name="inputAstNodes"></param>
        /// <returns></returns>
        public override IEnumerable<AssociativeNode> BuildOutputAst(List<AssociativeNode> inputAstNodes)
        {
            AssociativeNode node;

            if (SelectedIndex == -1)
            {
                node = AstFactory.BuildNullNode();
            }
            else
            {
                if (!(Items[SelectedIndex].Item is Element Element))
                {
                    node = AstFactory.BuildNullNode();
                }
                else
                {
                    var idNode = AstFactory.BuildStringNode(Element.UniqueId);
                    var falseNode = AstFactory.BuildBooleanNode(true);

                    node = AstFactory.BuildFunctionCall(
                        new Func<string, bool, object>(ElementSelector.ByUniqueId),
                        new List<AssociativeNode> { idNode, falseNode });
                }
            }

            return new[] { AstFactory.BuildAssignment(GetAstIdentifierForOutputIndex(0), node) };
        }
    }
}

﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using CoreNodeModels;
using DSRevitNodesUI;
using Dynamo.Graph.Nodes;
using Dynamo.Utilities;
using Newtonsoft.Json;
using ProtoCore.AST.AssociativeAST;
using RevitServices.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using ElementSelector = Revit.Elements.ElementSelector;

namespace BIMSharkDynamoToolsUI.Automation
{
    /// <summary>
    /// dfgdg
    /// </summary>
    [NodeName("BSSharedParameterElementInstanse")]
    [NodeCategory("BIMSharkDynamoTools.Revit")]
    [NodeDescription("Retrieve all available BIM Shark Shared Parameter Elements linked to instances.")]
    [IsDesignScriptCompatible]
    public class BSSharedParameterElementInstanseUi : RevitDropDownBase
    {
        /// <summary>
        /// hjhk
        /// </summary>
        /// <param name="inPorts"></param>
        /// <param name="outPorts"></param>
        [JsonConstructor]
        public BSSharedParameterElementInstanseUi(IEnumerable<PortModel> inPorts, IEnumerable<PortModel> outPorts) : base("SharedParameterElement", inPorts, outPorts) { }

        /// <summary>
        /// hjhk
        /// </summary>
        public BSSharedParameterElementInstanseUi() : base("SharedParameterElement") { }

        /// <summary>
        /// dfgfdg
        /// </summary>
        /// <param name="currentSelection"></param>
        protected override SelectionState PopulateItemsCore(string currentSelection)
        {
            Items.Clear();

            var vtList = new Autodesk.Revit.DB.FilteredElementCollector(DocumentManager.Instance.CurrentDBDocument)
                .OfClass(typeof(Autodesk.Revit.DB.SharedParameterElement))
                .Cast<Autodesk.Revit.DB.SharedParameterElement>()
                .Where(x => x.Name.Contains("BS ")).ToList();

            if (vtList.Count == 0)
            {
                Items.Add(new DynamoDropDownItem("No Parameters were found.", null));
                SelectedIndex = 0;
                return SelectionState.Done;
            }

            foreach (var v in vtList)
            {
                BindingMap map = DocumentManager.Instance.CurrentDBDocument.ParameterBindings;
                DefinitionBindingMapIterator it = map.ForwardIterator();
                it.Reset();

                //var foundparameters = new List<int>();
                while (it.MoveNext())
                {
                    Binding b = (ElementBinding)it.Current;
                    Definition definition = it.Key;
                    string name = definition.Name;
                    // Check for correct InstanceBinding
                    if (b.GetType() == typeof(InstanceBinding) && name == v.Name)
                    {
                        Items.Add(new DynamoDropDownItem(v.Name, v));
                    }
                }
            }
            Items = Items.OrderBy(x => x.Name).ToObservableCollection();
            return SelectionState.Restore;
        }

        /// <summary>
        /// dfgdg
        /// </summary>
        /// <param name="inputAstNodes"></param>
        public override IEnumerable<AssociativeNode> BuildOutputAst(List<AssociativeNode> inputAstNodes)
        {
            AssociativeNode node;

            if (SelectedIndex == -1)
            {
                node = AstFactory.BuildNullNode();
            }
            else
            {
                if (!(Items[SelectedIndex].Item is Autodesk.Revit.DB.SharedParameterElement SharedParameterElement))
                {
                    node = AstFactory.BuildNullNode();
                }
                else
                {
                    var idNode = AstFactory.BuildStringNode(SharedParameterElement.UniqueId);
                    var falseNode = AstFactory.BuildBooleanNode(true);

                    node = AstFactory.BuildFunctionCall(
                        new Func<string, bool, object>(ElementSelector.ByUniqueId),
                        new List<AssociativeNode> { idNode, falseNode });
                }
            }

            return new[] { AstFactory.BuildAssignment(GetAstIdentifierForOutputIndex(0), node) };
        }
    }
}

﻿using Autodesk.Revit.UI;
using BIMSharkDynamoToolsZeroTouch.Object.BSError;
using BIMSharkDynamoToolsZeroTouch.Object.Revit;
using BIMSharkDynamoToolsZeroTouch.Utils;
using RevitServices.Persistence;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using adWin = Autodesk.Windows;

namespace BIMSharkDynamoToolsZeroTouch.Utilities
{
    /// <summary>
    /// Random related utilities.
    /// </summary>
    public class Utilities
    {
        private const string password = "j^6Ci@jb%iHR";
        internal Utilities()
        {
        }

        /// <summary>
        /// Returns CommandKey form list of commands.
        /// </summary>
        /// <returns name="key">CommandKey.</returns>
        public static List<string> GetCommandKey(List<Command> Command)
        {
            List<string> keylist = new List<string>();

            foreach (Command item in Command)
            {
                string key = item.Key;
                bool check = keylist.Contains(key);
                if (!check)
                {
                    keylist.Add(key);
                }
            }

            if (keylist?.Count == 0)
            {
                return null;
            }
            return keylist;
        }

        /// <summary>
        /// Returns CommandId form list of commands.
        /// </summary>
        /// <returns name="CommandId">CommandId.</returns>
        public static List<string> GetCommandId(List<Command> Command)
        {
            List<string> CommandIdlist = new List<string>();

            foreach (Command item in Command)
            {
                string commandId = item.CommandId;
                bool check = CommandIdlist.Contains(commandId);
                if (!check)
                {
                    CommandIdlist.Add(commandId);
                }
            }

            if (CommandIdlist?.Count == 0)
            {
                return null;
            }
            return CommandIdlist;
        }

        /// <summary>
        /// Finds you BIM Shark API key and  return it as an encrypted string.
        /// </summary>
        /// <returns name="APIKey">Encrypted string.</returns>
        public static string GetBIMSharkAPIKey()
        {
            //var doc = DocumentManager.Instance.CurrentDBDocument;
            var uiapp = DocumentManager.Instance.CurrentUIApplication;
            //var app = uiapp.Application;

            var ribbonPanels = uiapp.ActiveUIDocument.Application.GetRibbonPanels("BIM Shark");

            Autodesk.Revit.UI.RibbonPanel assemblyLocation = ribbonPanels[0];

            string assemblyName = ((Autodesk.Revit.UI.PushButton)assemblyLocation.GetItems()[0]).AssemblyName;
            string currentAssemblyLocation = Path.GetDirectoryName(assemblyName);
            string apikey = GetSavedApiKey(currentAssemblyLocation);

            if (apikey?.Length != 32)
            {
                return null;
            }
            return apikey;
        }

        /// <summary>
        /// Finds the BIM Shark dll location.
        /// </summary>
        /// <returns name="Location">String.</returns>
        public static string GetBIMSharkDllLocation()
        {
            //var doc = DocumentManager.Instance.CurrentDBDocument;
            var uiapp = DocumentManager.Instance.CurrentUIApplication;
            //var app = uiapp.Application;

            var ribbonPanels = uiapp.ActiveUIDocument.Application.GetRibbonPanels("BIM Shark");

            Autodesk.Revit.UI.RibbonPanel assemblyLocation = ribbonPanels[0];

            string assemblyName = ((Autodesk.Revit.UI.PushButton)assemblyLocation.GetItems()[0]).AssemblyName;
            string currentAssemblyLocation = Path.GetDirectoryName(assemblyName);
            string file = currentAssemblyLocation + "\\BIMSharkRevitTools.dll";

            if (!File.Exists(file))
            {
                throw new ArgumentException("run needs to be a \"Boolean\" value");
            }

            return file;
        }

        internal static string GetSavedApiKey(string currentAssemblyLocation)
        {
            string apiKey = string.Empty;
            string user = Environment.UserName;
            //string currentAssemblyLocation = Assembly.GetExecutingAssembly().Location;
            //string folder1 = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\BIMShark\BIMSharkRevitTools\" + Application.m_RevitVersion + @"\Resources\";
            string folder1 = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\BIMShark\BIMSharkRevitTools\Resources\";
            string folder2 = Path.GetDirectoryName(currentAssemblyLocation) + @"\Resources\";
            string file = user + "BSAPI.bs";

            // Open the stream and read it back.
            if (File.Exists(folder1 + file))
            {
                using (StreamReader sr = File.OpenText(folder1 + file))
                {
                    string s = string.Empty;
                    //! ADD a Check to see if user is correct to automatically get the API.
                    //while ((s = sr.ReadLine()) != null)
                    //{
                    //    apiKey = SwichText(s);
                    //}
                    s = sr.ReadLine();
                    apiKey = StringCipher.Decrypt(s, password);

                    sr.Dispose();
                    sr.Close();
                }
            }
            else
            {
                if (File.Exists(folder2 + file))
                {
                    using (StreamReader sr = File.OpenText(folder2 + file))
                    {
                        string s = string.Empty;
                        //! ADD a Check to see if user is correct to automatically get the API.
                        //while ((s = sr.ReadLine()) != null)
                        //{
                        //    apiKey = SwichText(s);
                        //}
                        s = sr.ReadLine();
                        apiKey = StringCipher.Decrypt(s, password);

                        sr.Dispose();
                        sr.Close();
                    }
                }
            }
            if (apiKey == "Enter API key here")
            {
                return string.Empty;
            }
            else
            {
                return apiKey;
            }
        }

        internal static void SaveApiKey(string enteredapikey, string currentAssemblyLocation)
        {
            string user = Environment.UserName;
            //string currentAssemblyLocation = Assembly.GetExecutingAssembly().Location;
            //string folder1 = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\BIMShark\BIMSharkRevitTools\" + Application.m_RevitVersion + @"\Resources\";
            string folder1 = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\BIMShark\BIMSharkRevitTools\Resources\";
            string folder2 = Path.GetDirectoryName(currentAssemblyLocation) + @"\Resources\";
            string file = user + "BSAPI.bs";
            //SecureString sec_strPassword = new SecureString();
            try
            {
                if (!Directory.Exists(folder1))
                    Directory.CreateDirectory(folder1);

                using (StreamWriter sw = File.CreateText(folder1 + file))
                {
                    //sw.WriteLine(SwichText(enteredapikey));
                    string encryptedstring = StringCipher.Encrypt(enteredapikey, password);
                    sw.WriteLine(encryptedstring);

                    sw.Dispose();
                    //sw.Flush();
                    sw.Close();
                }
            }
            catch (NullReferenceException)
            {
                try
                {
                    if (!Directory.Exists(folder2))
                        Directory.CreateDirectory(folder2);

                    using (StreamWriter sw = File.CreateText(folder2 + file))
                    {
                        //sw.WriteLine(SwichText(enteredapikey));
                        string encryptedstring = StringCipher.Encrypt(enteredapikey, password);
                        sw.WriteLine(encryptedstring);

                        sw.Dispose();
                        //sw.Flush();
                        sw.Close();
                    }
                }
                catch (Exception)
                {
                    throw new ArgumentException("There was an error trying to save the AIP-key");
                }
            }
        }

        /// <summary>
        /// Use this node to remove "BSUpdater". BSUpdater is used when duplicating types found in the BIM Shark Revit addin.
        /// </summary>
        /// <param name="Run">Set to True to run the node.</param>
        /// <returns>contain a message and bool indicating whether the process was ran correctly.</returns>
        public static Dictionary<string, object> UpdaterRemove(object Run)
        {
            bool run = Utils.SampleUtilities.BoolParser(Run);

            var uiapp = DocumentManager.Instance.CurrentUIApplication;

            //var loadedApplications = uiapp.LoadedApplications;

            BSError StatusObj = new BSError
            {
                Message = "Set run to true",
                Worked = false,
                ThreadIsActive = false
            };

            if (run)
            {
                try
                {
                    // Get source ribbon panel
                    const string sourceTabName = "BIM Shark";
                    const string sourcePanelName = "More";
                    const string sourceRibbonItemName = "Remove BSUpdater";
                    string name_addin_button_cmd = string.Empty;

                    //adWin.RibbonItem sourceRibbonItem = null;

                    bool isBreak = false;
                    foreach (adWin.RibbonTab tab in adWin.ComponentManager.Ribbon.Tabs)
                    {
                        if (tab.Id.Equals(sourceTabName, StringComparison.OrdinalIgnoreCase))
                        {
                            foreach (adWin.RibbonPanel panel in tab.Panels)
                            {
                                if (panel.Source.AutomationName.Equals(sourcePanelName, StringComparison.OrdinalIgnoreCase))
                                {
                                    foreach (adWin.RibbonItem item in panel.Source.Items)
                                    {
                                        if (item.AutomationName == sourceRibbonItemName)
                                        {
                                            item.ShowText = false;
                                            item.IsVisible = true;
                                            item.IsEnabled = true;
                                            name_addin_button_cmd = item.Id;
                                            isBreak = true;
                                            break;
                                        }
                                    }
                                }
                                if (isBreak) break;
                            }
                            if (isBreak) break;
                        }
                    }

                    //const string name_addin_button_cmd = "CustomCtrl_%CustomCtrl_%BIM Shark%More%ButtonNameUpdaterRemove";

                    if (name_addin_button_cmd.Length > 0)
                    {
                        RevitCommandId id_addin_button_cmd
                          = RevitCommandId.LookupCommandId(
                            name_addin_button_cmd);

                        uiapp.PostCommand(id_addin_button_cmd);

                        StatusObj.Worked = true;
                        StatusObj.Message = "worked";
                    }
                    else
                    {
                        StatusObj.Worked = false;
                        StatusObj.Message = "Something went wrong. Try again later";
                    }
                }
                catch (Exception)
                {
                    StatusObj.Worked = false;
                    StatusObj.Message = "Something went wrong. Try again later";
                }
            }

            return new Dictionary<string, object>
            {
                { "FormClosed", StatusObj.Worked },
                { "Status", StatusObj.Message }
            };
        }

        /// <summary>
        /// Use this node to add "BSUpdater". BSUpdater is used when duplicating types found in the BIM Shark Revit addin.
        /// </summary>
        /// <param name="Run">Set to True to run the node.</param>
        /// <returns>contain a message and bool indicating whether the process was ran correctly.</returns>
        public static Dictionary<string, object> UpdaterRemoveAdd(object Run)
        {
            bool run = Utils.SampleUtilities.BoolParser(Run);

            var uiapp = DocumentManager.Instance.CurrentUIApplication;

            //var loadedApplications = uiapp.LoadedApplications;

            BSError StatusObj = new BSError
            {
                Message = "Set run to true",
                Worked = false,
                ThreadIsActive = false
            };

            if (run)
            {
                try
                {
                    // Get source ribbon panel
                    const string sourceTabName = "BIM Shark";
                    const string sourcePanelName = "More";
                    const string sourceRibbonItemName = "Add BSUpdater";
                    string name_addin_button_cmd = string.Empty;

                    //adWin.RibbonItem sourceRibbonItem = null;

                    bool isBreak = false;
                    foreach (adWin.RibbonTab tab in adWin.ComponentManager.Ribbon.Tabs)
                    {
                        if (tab.Id.Equals(sourceTabName, StringComparison.OrdinalIgnoreCase))
                        {
                            foreach (adWin.RibbonPanel panel in tab.Panels)
                            {
                                if (panel.Source.AutomationName.Equals(sourcePanelName, StringComparison.OrdinalIgnoreCase))
                                {
                                    foreach (adWin.RibbonItem item in panel.Source.Items)
                                    {
                                        if (item.AutomationName == sourceRibbonItemName)
                                        {
                                            item.ShowText = false;
                                            item.IsVisible = true;
                                            item.IsEnabled = true;
                                            name_addin_button_cmd = item.Id;
                                            isBreak = true;
                                            break;
                                        }
                                    }
                                }
                                if (isBreak) break;
                            }
                            if (isBreak) break;
                        }
                    }

                    if (name_addin_button_cmd.Length > 0)
                    {
                        RevitCommandId id_addin_button_cmd
                          = RevitCommandId.LookupCommandId(
                            name_addin_button_cmd);

                        uiapp.PostCommand(id_addin_button_cmd);

                        StatusObj.Worked = true;
                        StatusObj.Message = "worked";
                    }
                    else
                    {
                        StatusObj.Worked = false;
                        StatusObj.Message = "Something went wrong. Try again later";
                    }
                }
                catch (Exception)
                {
                    StatusObj.Worked = false;
                    StatusObj.Message = "Something went wrong. Try again later";
                }
            }

            return new Dictionary<string, object>
            {
                { "FormClosed", StatusObj.Worked },
                { "Status", StatusObj.Message }
            };
        }

        /// <summary>
        /// Use this node to....
        /// </summary>
        /// <param name="input"></param>
        /// <param name="trim"></param>
        /// <returns>....</returns>
        public static string RemoveNumbersAtEndOfString(string input, bool trim = false, bool RemoveHyphenAndUnderscore = true)
        {
            if (input == null)
            {
                return null;
            }

            const string pattern = @"\d+$";
            const string replacement = "";
            Regex rgx = new Regex(pattern);

            string output = rgx.Replace(input, replacement);

            if (RemoveHyphenAndUnderscore)
            {
                if (trim)
                {
                    output = output.Trim();
                }

                if (output.StartsWith("-"))
                {
                    output = output.Remove(0, 1);
                }
                if (output.StartsWith("_"))
                {
                    output = output.Remove(0, 1);
                }
                if (output.StartsWith("-"))
                {
                    output = output.Remove(0, 1);
                }
            }

            if (trim)
            {
                return output.Trim();
            }
            return output;
        }

        /// <summary>
        /// Use this node to....
        /// </summary>
        /// <param name="input"></param>
        /// <param name="trim"></param>
        /// <returns>....</returns>
        public static string RemoveNumbersAtBeginningOfString(string input, bool trim = false, bool RemoveHyphenAndUnderscore = true)
        {
            if (input == null)
            {
                return null;
            }

            char[] charArray = input.ToCharArray();
            Array.Reverse(charArray);
            string reverseinput = new string(charArray);

            const string pattern = @"\d+$";
            const string replacement = "";
            Regex rgx = new Regex(pattern);

            string reversestring = rgx.Replace(reverseinput, replacement);

            char[] finalcharArray = reversestring.ToCharArray();
            Array.Reverse(finalcharArray);
            string output = new string(finalcharArray);

            if (RemoveHyphenAndUnderscore)
            {
                if (trim)
                {
                    output = output.Trim();
                }

                if (output.StartsWith("-"))
                {
                    output = output.Remove(0, 1);
                }
                if (output.StartsWith("_"))
                {
                    output = output.Remove(0, 1);
                }
                if (output.StartsWith("-"))
                {
                    output = output.Remove(0, 1);
                }
            }

            if (trim)
            {
                return output.Trim();
            }

            return output;
        }
    }
}

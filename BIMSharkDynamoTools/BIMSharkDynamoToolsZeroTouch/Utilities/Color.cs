﻿using System;
using System.Collections.Generic;
using System.Drawing;
//using Autodesk.Revit.DB;

namespace BIMSharkDynamoToolsZeroTouch.Utilities
{
    /// <summary>
    /// Generats
    /// </summary>
    public static class BSColor
    {
        private static readonly Random rnd = new Random();

        /// <summary>
        /// Generats a list of random colors.
        /// </summary>
        /// <returns name="Color">Encrypted string.</returns>
        public static List<Color> GetRandomColor(int count)
        {
            if (count == 0)
            {
                return null;
            }

            List<Color> randomColorlist = new List<Color>();

            while (randomColorlist.Count < count)
            {
                Color randomColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                bool check = randomColorlist.Contains(randomColor);
                if (!check)
                {
                    randomColorlist.Add(randomColor);
                }
            }

            return randomColorlist;
        }
    }
}

﻿using Autodesk.Revit.DB;

namespace BIMSharkDynamoToolsZeroTouch.Revit
{
    // This class implements the ICentralLockedCallback to override Revit's default
    // behavior for how to handle when the central model is locked by another user
    internal class SynchLockCallback : ICentralLockedCallback
    {
        // If unable to lock central, give up rather than waiting
        public bool ShouldWaitForLockAvailability()
        {
            return false;
        }
    }
}
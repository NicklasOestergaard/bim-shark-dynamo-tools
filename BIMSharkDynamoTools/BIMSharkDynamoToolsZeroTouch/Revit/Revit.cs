﻿using Autodesk.DesignScript.Runtime;
using Autodesk.Revit.DB;
using BIMSharkDynamoToolsZeroTouch.Object.Revit;
using RevitServices.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using BIMSharkDynamoToolsZeroTouch.Utils;
using Autodesk.Revit.UI;
using Revit.Elements;
using System.Threading;
using Category = Revit.Elements.Category;
using Revit.Elements.Views;
using DynView3D = Revit.Elements.Views.View3D;

namespace BIMSharkDynamoToolsZeroTouch.Revit
{
    /// <summary>
    /// Revit related utilities.
    /// </summary>
    public class Revit
    {
        internal Revit()
        {
        }
        /// <summary>
        /// Lists all BIM Shark Standard object categories.
        /// </summary>
        [MultiReturn(new[] { "BSTypeElements", "BSInstanceElements" })]
        public static Dictionary<string, object> GetAllBSTypesFromView(List<Category> BSCategories, DynView3D BS3DView)
        {
            var doc = DocumentManager.Instance.CurrentDBDocument;

            // Export Types via a 3D view
            Autodesk.Revit.DB.Element BS3DViewElement = null;
            if (BS3DView?.Id.ToString().Length > 2)
            {
                int idInt = Convert.ToInt32(BS3DView.Id);
                ElementId id = new ElementId(idInt);
                BS3DViewElement = doc.GetElement(id);
            }

            //FilteredElementCollector collector = new FilteredElementCollector(doc);

            List<ElementFilter> categoryFilters = new List<ElementFilter>();
            foreach (Category category in BSCategories)
            {
                categoryFilters.Add(new ElementCategoryFilter((BuiltInCategory)category.Id));
            }

            // Create a category filter
            ElementFilter filter = new LogicalOrFilter(categoryFilters);

            //IList<Autodesk.Revit.DB.Element> inselementList = null;
            IList<Autodesk.Revit.DB.Element> inselementList;

            if (BS3DViewElement != null)
            {
                // Export Instances via a 3D view
                inselementList = new FilteredElementCollector(doc, BS3DViewElement.Id).WhereElementIsNotElementType().WherePasses(filter).ToElements();
            }
            else
            {
                // Export Instances not via a 3D view
                inselementList = new FilteredElementCollector(doc).WhereElementIsNotElementType().WherePasses(filter).ToElements();
            }

            List<string> mytemp = new List<string>();
            mytemp.Clear();

            List<Autodesk.Revit.DB.Element> myBSTypeElements = new List<Autodesk.Revit.DB.Element>();
            List<object> outBSTypeElements = new List<object>();
            List<object> outBSInselements = new List<object>();

            myBSTypeElements.Clear();
            //Add the value to all element
            if (inselementList.Count > 0)
            {
                foreach (Autodesk.Revit.DB.Element el in inselementList)
                {
                    var element = doc.GetElement(el.Id);
                    outBSInselements.Add(element.ToDSType(false));

                    // Find type if any
                    Autodesk.Revit.DB.ElementType eType = GetType(el, doc);

                    // Elements that has a type
                    if (eType != null && !mytemp.Contains(eType.Id.IntegerValue.ToString()) && eType.IsValidObject)
                    {
                        mytemp.Add(eType.Id.IntegerValue.ToString());
                        var elementtype = doc.GetElement(eType.Id);
                        outBSTypeElements.Add(elementtype.ToDSType(false));
                    }
                }
            }

            return new Dictionary<string, object>
            {
                { "BSTypeElements", outBSTypeElements},
                { "BSInstanceElements", outBSInselements}
            };
        }

        /// <summary>
        /// Lists all BIM Shark Standard object categories.
        /// </summary>
        [MultiReturn(new[] { "BSTypeElements", "BSInstanceElements" })]
        public static Dictionary<string, object> GetAllBSTypes(List<Category> BSCategories)
        {
            var doc = DocumentManager.Instance.CurrentDBDocument;

            //FilteredElementCollector collector = new FilteredElementCollector(doc);

            List<ElementFilter> categoryFilters = new List<ElementFilter>();
            foreach (Category category in BSCategories)
            {
                categoryFilters.Add(new ElementCategoryFilter((BuiltInCategory)category.Id));
            }

            // Create a category filter
            ElementFilter filter = new LogicalOrFilter(categoryFilters);

            // Export Instances via a 3D view
            IList<Autodesk.Revit.DB.Element> inselementList = new FilteredElementCollector(doc).WhereElementIsNotElementType().WherePasses(filter).ToElements();

            List<string> mytemp = new List<string>();
            mytemp.Clear();

            List<Autodesk.Revit.DB.Element> myBSTypeElements = new List<Autodesk.Revit.DB.Element>();
            List<object> outBSTypeElements = new List<object>();
            List<object> outBSInselements = new List<object>();

            myBSTypeElements.Clear();
            //Add the value to all element
            if (inselementList.Count > 0)
            {
                foreach (Autodesk.Revit.DB.Element el in inselementList)
                {
                    var element = doc.GetElement(el.Id);
                    outBSInselements.Add(element.ToDSType(false));

                    // Find type if any
                    Autodesk.Revit.DB.ElementType eType = GetType(el, doc);

                    // Elements that has a type
                    if (eType != null && !mytemp.Contains(eType.Id.IntegerValue.ToString()) && eType.IsValidObject)
                    {
                        mytemp.Add(eType.Id.IntegerValue.ToString());
                        var elementtype = doc.GetElement(eType.Id);
                        outBSTypeElements.Add(elementtype.ToDSType(false));
                    }
                }
            }

            return new Dictionary<string, object>
            {
                { "BSTypeElements", outBSTypeElements},
                { "BSInstanceElements", outBSInselements}
            };
        }
        private static Autodesk.Revit.DB.ElementType GetType(Autodesk.Revit.DB.Element el, Document Doc)
        {
            Autodesk.Revit.DB.ElementType eType = null;

            if (!el.IsValidObject)
            {
                return eType;
            }

            if (Doc.GetElement(el.GetTypeId()) is Autodesk.Revit.DB.ElementType)
            {
                eType =Doc.GetElement(el.GetTypeId()) as Autodesk.Revit.DB.ElementType;
            }
            return eType;
        }

        /// <summary>
        /// Lists all BIM Shark Standard object categories.
        /// </summary>
        /// <returns name="Category">ALL Shark Standard object categorie.</returns>
        public static List<Category> GetAllBSStandardObjectCategories()
        {
            var doc = DocumentManager.Instance.CurrentDBDocument;
            //var e = element.InternalElement as Autodesk.Revit.DB.ElementType;

            var speList = new Autodesk.Revit.DB.FilteredElementCollector(DocumentManager.Instance.CurrentDBDocument)
                .OfClass(typeof(Autodesk.Revit.DB.SharedParameterElement))
                .Cast<Autodesk.Revit.DB.SharedParameterElement>()
                .Where(x => x.Name.Contains("BS Component Type Id")).ToList();

            if (speList?.Count == 0)
            {
                return null;
            }

            List<Category> BSStandardCategorieList = new List<Category>();

            BindingMap map = DocumentManager.Instance.CurrentDBDocument.ParameterBindings;
            DefinitionBindingMapIterator it = map.ForwardIterator();
            it.Reset();

            var foundparameters = new List<int>();
            while (it.MoveNext())
            {
                Definition definition = it.Key;
                string name = definition.Name;
                if (name == "BS Component Type Id")
                {
                    CategorySet CategorySet = ((ElementBinding)it.Current).Categories;
                    CategorySetIterator CategorItor = CategorySet.ForwardIterator();

                    while (CategorItor.MoveNext())
                    {
                        Autodesk.Revit.DB.Category Cat = CategorItor.Current as Autodesk.Revit.DB.Category;

                        //var element = doc.GetElement(Cat.Id);
                        var element = Category.ById(Cat.Id.IntegerValue);

                        if (!BSStandardCategorieList.Contains(element))
                        {
                            BSStandardCategorieList.Add(element);
                        }
                    }
                    break;
                }
            }

            return BSStandardCategorieList;
        }

        /// <summary>
        /// Lists all BIM Shark Parameters found in document.
        /// </summary>
        /// <returns name="SharedParameterElement">ALL BIM Shark Parameters.</returns>
        public static List<Autodesk.Revit.DB.SharedParameterElement> GetAllBSSharedParameters()
        {
            var doc = DocumentManager.Instance.CurrentDBDocument;
            //var e = element.InternalElement as Autodesk.Revit.DB.ElementType;

            var speList = new Autodesk.Revit.DB.FilteredElementCollector(DocumentManager.Instance.CurrentDBDocument)
                .OfClass(typeof(Autodesk.Revit.DB.SharedParameterElement))
                .Cast<Autodesk.Revit.DB.SharedParameterElement>()
                .Where(x => x.Name.Contains("BS ")).ToList();

            if (speList?.Count == 0)
            {
                return null;
            }
            return speList;
        }

        /// <summary>
        /// Lists all BIM Shark Commands
        /// </summary>
        public static List<object> GetAllBSCommands()
        {
            //var doc = DocumentManager.Instance.CurrentDBDocument;
            var uiapp = DocumentManager.Instance.CurrentUIApplication;
            //var app = uiapp.Application;

            var ribbonPanels = uiapp.ActiveUIDocument.Application.GetRibbonPanels("BIM Shark");
            //var loadedApplications = uiapp.ActiveUIDocument.Application.LoadedApplications;

            List<object> allRibbons = new List<object>();
            List<string> checklist = new List<string>();

            List<Autodesk.Revit.UI.RibbonItemType> types = new List<Autodesk.Revit.UI.RibbonItemType>();

            //"CustomCtrl_%CustomCtrl_%BIM Shark%

            foreach (Autodesk.Revit.UI.RibbonPanel RibbonPanel in ribbonPanels)
            {
                string ribbonpanelName = RibbonPanel.Name;

                //string ribbonpanelText = RibbonPanel.Title;
                foreach (Autodesk.Revit.UI.RibbonItem ribbonItem in RibbonPanel.GetItems())
                {
                    string itemName = ribbonItem.Name;
                    //string itemText = item.ItemText;

                    types.Add(ribbonItem.ItemType);

                    if (ribbonItem.ItemType == Autodesk.Revit.UI.RibbonItemType.SplitButton)
                    {
                        foreach (var pushButton in ((Autodesk.Revit.UI.SplitButton)ribbonItem).GetItems())
                        {
                            string pushButtoncommandId = string.Format("CustomCtrl_%CustomCtrl_%BIM Shark%{0}%{1}%{2}", ribbonpanelName, itemName, pushButton.Name);
                            string pushButtonkey = string.Format("{0}-{1}-{2} ", ribbonpanelName, itemName, pushButton.Name);

                            if (!checklist.Contains(pushButtonkey))
                            {
                                checklist.Add(pushButtonkey);
                                Command dictionary = new Command
                                {
                                    Key = pushButtonkey,
                                    CommandId = pushButtoncommandId,
                                };
                                allRibbons.Add(dictionary);
                            }
                        }
                    }
                    else
                    {
                        string commandId = string.Format("CustomCtrl_%CustomCtrl_%BIM Shark%{0}%{1}", ribbonpanelName, itemName);
                        string key = string.Format("{0}-{1}", ribbonpanelName, itemName);
                        //ribbonItem.Add(commandId);

                        if (!checklist.Contains(key))
                        {
                            checklist.Add(key);
                            Command dictionary = new Command
                            {
                                Key = key,
                                CommandId = commandId,
                            };
                            allRibbons.Add(dictionary);
                        }
                    }
                }
            }

            // Missing commands
            //"CustommCtrl_%CustomCtrl_%CustomCtrl_%BIM Shark%Import/Export%SplitButton1%ButtonNameLinkToDocuments";
            //"CustomCtrl_%CustomCtrl_%CustomCtrl_%BIM Shark%Import/Export%SplitButton1%ButtonNameSyncSchedule";
            //"CustomCtrl_%CustomCtrl_%CustomCtrl_%BIM Shark%Add Filters%splitButton1%ButtonNameBSComponentTypeId";
            //"CustomCtrl_%CustomCtrl_%CustomCtrl_%BIM Shark%Add Filters%splitButton1%ButtonNameBSComponentInstanceId";
            //"CustomCtrl_%CustomCtrl_%CustomCtrl_%BIM Shark%Add Filters%splitButton1%ButtonNameBSTender";
            //"CustomCtrl_%CustomCtrl_%CustomCtrl_%BIM Shark%Add Filters%splitButton1%ButtonNameBSInstanceTender";
            //"CustomCtrl_%CustomCtrl_%CustomCtrl_%BIM Shark%Add Filters%splitButton1%ButtonNameBSDocLink";
            //"CustomCtrl_%CustomCtrl_%CustomCtrl_%BIM Shark%Add Filters%splitButton1%ButtonNameBSInstanceDocLink";

            if (allRibbons?.Count == 0)
            {
                return null;
            }

            return allRibbons;
        }

        /// <summary>
        /// Save to central
        /// </summary>
        /// <param name="Run">Set to True to run the node.</param>
        /// <param name="compact">Save model as compact?</param>
        /// <param name="comment">Add a comment.</param>
        /// <param name="relinquish">Relinquish all on save.</param>
        /// <param name="savelocalafter">Save localfil after</param>
        /// <returns>contain a bool and error message indicating whether the process ran correctly.</returns>
        [MultiReturn(new[] { "Worked", "AnyError" })]
        [CLSCompliant(false)]
#pragma warning disable CS3021 // Type or member does not need a CLSCompliant attribute because the assembly does not have a CLSCompliant attribute
        public static Dictionary<string, object> SaveToCentral(object Run, bool compact = false, string comment = "", bool relinquish = true, bool savelocalafter = false)
#pragma warning restore CS3021 // Type or member does not need a CLSCompliant attribute because the assembly does not have a CLSCompliant attribute
        {
            bool run = SampleUtilities.BoolParser(Run);
            if (!run)
            {
                return new Dictionary<string, object>
                {
                    { "Worked", false },
                    { "AnyError", "Set run to true"}
                };
            }

            var doc = DocumentManager.Instance.CurrentDBDocument;
            //var uiapp = DocumentManager.Instance.CurrentUIApplication;
            //var app = uiapp.Application;

            // Set options for accessing central model
            TransactWithCentralOptions transOpts = new TransactWithCentralOptions();
            SynchLockCallback transCallBack = new SynchLockCallback();
            // Override default behavior of waiting to try again if the central model is locked
            transOpts.SetLockCallback(transCallBack);

            // Set options for synchronizing with central
            SynchronizeWithCentralOptions syncOpts = new SynchronizeWithCentralOptions();
            // Sync without relinquishing any checked out elements or worksets
            RelinquishOptions relinquishOpts = new RelinquishOptions(relinquish);
            syncOpts.SetRelinquishOptions(relinquishOpts);
            // Do not automatically save local model after sync
            syncOpts.Compact = compact;
            syncOpts.SaveLocalAfter = savelocalafter;
            syncOpts.Comment = comment;

            try
            {
                doc.SynchronizeWithCentral(transOpts, syncOpts);
                return new Dictionary<string, object>
                {
                    { "Worked", true },
                    { "AnyError", "" }
                };
            }
            catch (Exception ex)
            {
                return new Dictionary<string, object>
                {
                    { "Worked", false },
                    { "AnyError", string.Format("Error: {0}", ex.Message) }
                };
            }
        }

        /// <summary>
        /// Save document
        /// </summary>
        /// <param name="Run">Set to True to run the node.</param>
        /// <param name="compact">Save model as compact?.</param>
        /// <returns>contain a bool and error message indicating whether the process ran correctly.</returns>
        [MultiReturn(new[] { "Worked", "AnyError" })]
        [CLSCompliant(false)]
#pragma warning disable CS3021 // Type or member does not need a CLSCompliant attribute because the assembly does not have a CLSCompliant attribute
        public static Dictionary<string, object> SaveDocument(object Run, bool compact = false)
#pragma warning restore CS3021 // Type or member does not need a CLSCompliant attribute because the assembly does not have a CLSCompliant attribute
        {
            bool run = SampleUtilities.BoolParser(Run);
            if (!run)
            {
                return new Dictionary<string, object>
                {
                    { "Worked", false },
                    { "AnyError", "Set run to true"}
                };
            }

            var doc = DocumentManager.Instance.CurrentDBDocument;
            //var uiapp = DocumentManager.Instance.CurrentUIApplication;
            //var app = uiapp.Application;

            // Set options for synchronizing with central
            SaveOptions saveOpts = new SaveOptions
            {
                Compact = compact
            };

            try
            {
                doc.Save(saveOpts);
                return new Dictionary<string, object>
                {
                    { "Worked", true },
                    { "AnyError", "" }
                };
            }
            catch (Exception ex)
            {
                return new Dictionary<string, object>
                {
                    { "Worked", false },
                    { "AnyError", string.Format("Error: {0}", ex.Message) }
                };
            }
        }

        /// <summary>
        /// Identifies if worksharing (i.e. editing permissions and multiple worksets) have been enabled in the document.
        /// </summary>
        /// <returns>Bool, true if on, false if off.</returns>
        [CLSCompliant(false)]
#pragma warning disable CS3021 // Type or member does not need a CLSCompliant attribute because the assembly does not have a CLSCompliant attribute
        public static bool Workshared()
#pragma warning restore CS3021 // Type or member does not need a CLSCompliant attribute because the assembly does not have a CLSCompliant attribute
        {
            var doc = DocumentManager.Instance.CurrentDBDocument;
            //var uiapp = DocumentManager.Instance.CurrentUIApplication;
            //var app = uiapp.Application;

            return doc.IsWorkshared;
        }

        /// <summary>
        /// Lists all BIM Shark Commands
        /// </summary>
        [MultiReturn(new[] { "SelectedSheets", "VisibelInBrowser", "NotFoudElements" })]
        public static Dictionary<string, List<object>> GetSeletedElementFromProjectBrowser()
        {
            UIApplication uiapp = DocumentManager.Instance.CurrentUIApplication;
            UIDocument uiDoc = uiapp.ActiveUIDocument;
            Document doc = DocumentManager.Instance.CurrentDBDocument;

            ICollection<ElementId> CurrentSelectedIds = uiDoc.Selection.GetElementIds();

            if (CurrentSelectedIds.Count == 0 )
            {
                return null;
            }

            Func<Autodesk.Revit.DB.View> projectBrowser = new FilteredElementCollector(doc).OfClass(typeof(Autodesk.Revit.DB.View))
                .Cast<Autodesk.Revit.DB.View>()
                .Where(x => x.ViewType == ViewType.ProjectBrowser).FirstOrDefault;

            BrowserOrganization org = BrowserOrganization.GetCurrentBrowserOrganizationForViews(doc);

            List<object> selectedsheets = new List<object>();
            List<object> visibelinbrowser = new List<object>();
            List<object> notfoudelements = new List<object>();

            foreach (ElementId ele in CurrentSelectedIds)
            {
                var element = doc.GetElement(ele);
                if (element.Category.Name == "Sheets")
                {
                    selectedsheets.Add(element.ToDSType(false));
                    visibelinbrowser.Add(org.AreFiltersSatisfied(ele));
                }
                else
                {
                    notfoudelements.Add(element.ToDSType(false));
                }
            }

            return new Dictionary<string, List<object>>
            {
                { "SelectedSheets", selectedsheets },
                { "VisibelInBrowser", visibelinbrowser },
                { "NotFoudElements", notfoudelements }
            };

            //List<FolderItemInfo> folderfields = org.GetFolderItems(CurrentSelectedIds.First()).ToList();
            //foreach (FolderItemInfo info in folderfields)
            //{
            //    // "groupheader" in browser == parameter value for grouping parameter
            //    string groupheader = info.Name;
            //    // parameterId for the grouping parameter
            //    // parameterId <0  parameter is BuiltInParameter
            //    ElementId parameterId = info.ElementId;
            //}
        }

        /// <summary>
        /// Close Active Document by pressing F4
        /// </summary>
        public static bool? CloseActiveDocument()
        {
            UIApplication uiapp = DocumentManager.Instance.CurrentUIApplication;
            UIDocument uiDoc = uiapp.ActiveUIDocument;
            Document doc = DocumentManager.Instance.CurrentDBDocument;

            //Document pDoc = commandData.Application
            //  .ActiveUIDocument.Document;

            if (doc != null)
            {
                ThreadPool.QueueUserWorkItem(
                    new WaitCallback(CloseDocProc));

                            return true;
            }

            return false;
        }

        private static void CloseDocProc(object state)
        {
            try
            {
                // maybe we need some checks for the right
                // document, but this is a simple sample...

                System.Windows.Forms.SendKeys.SendWait("^{F4}");
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}

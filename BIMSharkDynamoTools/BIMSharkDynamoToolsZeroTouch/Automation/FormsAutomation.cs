﻿using Autodesk.DesignScript.Runtime;
using Autodesk.Revit.DB;
using BIMSharkDynamoToolsZeroTouch.Object.BSError;
using Dynamo.Graph.Nodes;
using Revit.Elements.Views;
using RevitServices.Persistence;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Form = System.Windows.Forms.Form;

namespace BIMSharkDynamoToolsZeroTouch.Automation
{
    /// <summary>
    /// FormsAutomation
    /// </summary>
    public static class FormsAutomation
    {
        /// <summary>
        /// Use this node to run "Sync Now" process found in the BIM Shark Revit addin.
        /// </summary>
        /// <param name="Run">Set to True to run the node.</param>
        /// <param name="Dll">location of BIMSharkRevitTools.dll.</param>
        /// <returns>contain a message and bool indicating whether the process ran correctly.</returns>
        [NodeCategory("Query")]
        [MultiReturn(new[] { "FormClosed", "Status" })]
        public static Dictionary<string, object> CreateComponent(object Run, string Dll)
        {
            bool run = Utils.SampleUtilities.BoolParser(Run);

            BSError StatusObj = new BSError
            {
                Message = "Set run to true",
                Worked = false,
                ThreadIsActive = false
            };

            if (run)
            {
                Thread t = new Thread(() => StatusObj = SyncNowBackgroudThread(StatusObj));
                t.Start();
                StatusObj.ThreadIsActive = true;
                try
                {
                    Invokemethod(null,"test", "CommandApplicationSyncNow", Dll);

                    if (t.IsAlive)
                    {
                        t.Abort();
                        StatusObj.ThreadIsActive = false;
                    }

                    StatusObj.ThreadIsActive = false;
                    return new Dictionary<string, object>
                    {
                        { "FormClosed", StatusObj.Worked },
                        { "Status", StatusObj.Message }
                    };
                }
                catch (Exception ex)
                {
                    if (t.IsAlive)
                    {
                        t.Abort();
                        StatusObj.ThreadIsActive = false;
                    }
                    StatusObj.Worked = false;
                    StatusObj.Message = ex.Message;
                    return new Dictionary<string, object>
                    {
                        { "FormClosed", StatusObj.Worked },
                        { "Status", StatusObj.Message }
                    };
                }
            }

            return new Dictionary<string, object>
            {
                { "FormClosed", StatusObj.Worked },
                { "Status", StatusObj.Message }
            };
        }

        private static readonly object Autoruntext = "autorun";

        /// <summary>
        /// Use this node to run "Sync Now" process found in the BIM Shark Revit addin.
        /// </summary>
        /// <param name="Run">Set to True to run the node.</param>
        /// <param name="Dll">location of BIMSharkRevitTools.dll.</param>
        /// <returns>contain a message and bool indicating whether the process ran correctly.</returns>
        [NodeCategory("Query")]
        [MultiReturn(new[] { "FormClosed", "Status" })]
        public static Dictionary<string, object> SyncNow(object Run, string Dll)
        {
            bool run = Utils.SampleUtilities.BoolParser(Run);

            //WorkspaceModel currentWorkspace = DynamoRevit.RevitDynamoModel.CurrentWorkspace;
            var doc = DocumentManager.Instance.CurrentDBDocument;
            //var uiapp = DocumentManager.Instance.CurrentUIApplication;
            //var uiDoc = uiapp.ActiveUIDocument;
            //var app = uiapp.Application;

            // Find all Wall instances in the document by using category filter
            ElementCategoryFilter filter = new ElementCategoryFilter(BuiltInCategory.OST_ProjectInformation);

            // Apply the filter to the elements in the active document,
            // Use shortcut WhereElementIsNotElementType() to find wall instances only
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            Autodesk.Revit.DB.Element projectInformation = collector.WherePasses(filter).WhereElementIsNotElementType().ToElements()[0];

            ElementSet collection = new ElementSet();
            //foreach (ElementId elementId in uiDoc.Selection.GetElementIds())
            //{
                collection.Insert(projectInformation);
            //}

            BSError StatusObj = new BSError
            {
                Message = "Set run to true",
                Worked = false,
                ThreadIsActive = false
            };

            if (run)
            {
                Thread t = new Thread(() => StatusObj = SyncNowBackgroudThread(StatusObj));
                t.Start();
                StatusObj.ThreadIsActive = true;
                try
                {
                    Invoke(collection, "CommandApplicationSyncNow", Dll);
                    //while (!StatusObj.Worked || StatusObj.Message != "Unhandled alert dialog detected")
                    //{
                    //    Console.WriteLine("Running");
                    //}
                    if (t.IsAlive)
                    {
                        t.Abort();
                        StatusObj.ThreadIsActive = false;
                    }

                    FormCollection openforms = Application.OpenForms;
                    for (int i = 0; i < openforms.Count; i++)
                    {
                        System.Windows.Forms.Form form = openforms[i];

                        string formname = form?.Name;
                        //IntPtr formhandle = form.Handle;

                        if (form.ProductName == "BIMSharkRevitTools"
                            && formname == "SyncNowProgressForm" && form.Text == "BIM Shark - Sync Now Done")
                        {
                            StatusObj.Worked = true;
                            StatusObj.Message = "Worked";
                            CloseForm(form);
                            //if (form.Text.Contains("BIM Shark - Synchronizing Data"))
                            //{

                            //}
                        }
                    }
                    StatusObj.ThreadIsActive = false;
                    return new Dictionary<string, object>
                    {
                        { "FormClosed", StatusObj.Worked },
                        { "Status", StatusObj.Message }
                    };
                }
                catch (Exception ex)
                {
                    if (t.IsAlive)
                    {
                        t.Abort();
                        StatusObj.ThreadIsActive = false;
                    }
                    StatusObj.Worked = false;
                    StatusObj.Message = ex.Message;
                    return new Dictionary<string, object>
                    {
                        { "FormClosed", StatusObj.Worked },
                        { "Status", StatusObj.Message }
                    };
                }
            }

            return new Dictionary<string, object>
            {
                { "FormClosed", StatusObj.Worked },
                { "Status", StatusObj.Message }
            };
        }

        private static BSError SyncNowBackgroudThread(BSError StatusObj)
        {
            const int maxtime = 120 * 1000;
            const int sleep = 2 * 1000;
            int elapsed = 0;

            DateTime Tstart = DateTime.Now;
            DateTime Tmax = Tstart.AddMilliseconds(maxtime);

            while ((!StatusObj.Worked) && (StatusObj.Message != "Unhandled alert dialog detected") &&(DateTime.Compare(Tmax, Tstart) > 0))
            {
                DateTime Tcheck = Tstart.AddMilliseconds(elapsed);
                if (DateTime.Now >= Tcheck)
                {
                    const int nChars = 256;
                    StringBuilder Buff = new StringBuilder(nChars);

                    //IntPtr messagehandle = GetForegroundWindow();

                    //// Get the control's text.
                    //GetWindowCaption(messagehandle, Buff, nChars);

                    IntPtr AnyError = FindWindowByCaption(IntPtr.Zero,  "BIM Shark - Error");
                    if (AnyError != IntPtr.Zero)
                    {
                        bool worked = EnumChildWindows(AnyError, EnumChildWindowsCallbackError, IntPtr.Zero);
                        StatusObj.Worked = false;
                        StatusObj.Message = "Unhandled alert dialog detected";
                        //return statusObj;
                    }

                    IntPtr RunSync = FindWindowByCaption(IntPtr.Zero, "BIM Shark - Run Sync?");
                    if (RunSync != IntPtr.Zero)
                    {
                        bool worked = EnumChildWindows(RunSync, EnumChildWindowsCallbackRunSync, IntPtr.Zero);
                        //statusObj.Worked = false;
                        //statusObj.Message = "Unhandled alert dialog detected";
                        //return statusObj;
                    }

                    elapsed += sleep;
                }
            }

            return StatusObj;
        }

        /// <summary>
        /// Use this node to run "Sync Schedule" process found in the BIM Shark Revit addin.
        /// </summary>
        /// <param name="Run">Set to True to run the node.</param>
        /// <param name="Dll">location of BIMSharkRevitTools.dll.</param>
        /// <param name="Schedule">The schedule (view) to export.</param>
        /// <returns>contain a message and bool indicating whether the process was ran correctly.</returns>
        [NodeCategory("Query")]
        [MultiReturn(new[] { "FormClosed", "Status" })]
        public static Dictionary<string, object> SyncSchedule(object Run, string Dll, ScheduleView Schedule)
        {
            bool run = Utils.SampleUtilities.BoolParser(Run);

            var doc = DocumentManager.Instance.CurrentDBDocument;

            // Find all Wall instances in the document by using category filter
            ElementCategoryFilter filter = new ElementCategoryFilter(BuiltInCategory.OST_ProjectInformation);

            // Apply the filter to the elements in the active document,
            // Use shortcut WhereElementIsNotElementType() to find wall instances only
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            Autodesk.Revit.DB.Element projectInformation = collector.WherePasses(filter).WhereElementIsNotElementType().ToElements()[0];

            ElementSet collection = new ElementSet();
            //foreach (ElementId elementId in uiDoc.Selection.GetElementIds())
            //{
            collection.Insert(projectInformation);

            //string vendor_id = .;
            //int idInt = Convert.ToInt32(vendor_id);
            ElementId id = new ElementId(Schedule.Id);
            collection.Insert(doc.GetElement(id));
            //}

            BSError StatusObj = new BSError
            {
                Message = "Set run to true",
                Worked = false,
                ThreadIsActive = false
            };

            if (run)
            {
                Thread t = new Thread(() => StatusObj = SyncScheduleBackgroudThread(StatusObj));
                t.Start();
                StatusObj.ThreadIsActive = true;
                try
                {
                    Invoke(collection, "CommandApplicationSyncSchedule", Dll);
                    //while (!StatusObj.Worked || StatusObj.Message != "Unhandled alert dialog detected")
                    //{
                    //    Console.WriteLine("Running");
                    //}
                    if (t.IsAlive)
                    {
                        t.Abort();
                        StatusObj.ThreadIsActive = false;
                    }

                    IntPtr worked = FindWindowByCaption(IntPtr.Zero, "BIM Shark - Sync Schedule Done");
                    if (worked != IntPtr.Zero)
                    {
                        EnumChildWindows(worked, EnumChildWindowsCallbackError, IntPtr.Zero);
                        StatusObj.Worked = true;
                        StatusObj.Message = "Worked";
                    }

                    IntPtr ditnotwork = FindWindowByCaption(IntPtr.Zero, "BIM Shark - Sync Schedule Error");
                    if (ditnotwork != IntPtr.Zero)
                    {
                        EnumChildWindows(ditnotwork, EnumChildWindowsCallbackError, IntPtr.Zero);
                        StatusObj.Worked = false;
                        StatusObj.Message = "Something went wrong. Try again later";
                    }

                    //FormCollection openforms = Application.OpenForms;
                    //for (int i = 0; i < openforms.Count; i++)
                    //{
                    //    System.Windows.Forms.Form form = openforms[i];

                    //    string formname = form?.Name;
                    //    //IntPtr formhandle = form.Handle;

                    //    if (form.ProductName == "BIMSharkRevitTools"
                    //        && formname == "SyncNowProgressForm" && form.Text == "BIM Shark - Sync Now Done")
                    //    {
                    //        StatusObj.Worked = false;
                    //        StatusObj.Message = "worked";
                    //        CloseForm(form);
                    //        //if (form.Text.Contains("BIM Shark - Synchronizing Data"))
                    //        //{

                    //        //}
                    //    }
                    //}
                    StatusObj.ThreadIsActive = false;
                    return new Dictionary<string, object>
                    {
                        { "FormClosed", StatusObj.Worked },
                        { "Status", StatusObj.Message }
                    };
                }
                catch (Exception ex)
                {
                    if (t.IsAlive)
                    {
                        t.Abort();
                        StatusObj.ThreadIsActive = false;
                    }
                    StatusObj.Worked = false;
                    StatusObj.Message = ex.Message;
                    return new Dictionary<string, object>
                    {
                        { "FormClosed", StatusObj.Worked },
                        { "Status", StatusObj.Message }
                    };
                }
            }

            return new Dictionary<string, object>
            {
                { "FormClosed", StatusObj.Worked },
                { "Status", StatusObj.Message }
            };
        }

        /// <summary>
        /// Use this node to run "Sync Schedule" process found in the BIM Shark Revit addin.
        /// </summary>
        /// <param name="StatusObj">Status object.</param>
        /// <returns>contain a message and bool indicating whether the process ran correctly.</returns>
        [NodeCategory("Query")]
        [MultiReturn(new[] { "FormClosed", "Status" })]
        private static BSError SyncScheduleBackgroudThread(BSError StatusObj)
        {
            const int maxtime = 120 * 1000;
            const int sleep = 2 * 1000;
            int elapsed = 0;

            DateTime Tstart = DateTime.Now;
            DateTime Tmax = Tstart.AddMilliseconds(maxtime);

            while ((!StatusObj.Worked) && (StatusObj.Message != "Unhandled alert dialog detected") && (DateTime.Compare(Tmax, Tstart) > 0))
            {
                DateTime Tcheck = Tstart.AddMilliseconds(elapsed);
                if (DateTime.Now >= Tcheck)
                {
                    const int nChars = 256;
                    StringBuilder Buff = new StringBuilder(nChars);

                    //IntPtr messagehandle = GetForegroundWindow();

                    //// Get the control's text.
                    //GetWindowCaption(messagehandle, Buff, nChars);

                    IntPtr AnyError = FindWindowByCaption(IntPtr.Zero, "BIM Shark - Error");
                    if (AnyError != IntPtr.Zero)
                    {
                        bool worked = EnumChildWindows(AnyError, EnumChildWindowsCallbackError, IntPtr.Zero);
                        StatusObj.Worked = false;
                        StatusObj.Message = "Unhandled alert dialog detected";
                        //return statusObj;
                    }

                    IntPtr didworked = FindWindowByCaption(IntPtr.Zero, "BIM Shark - Sync Schedule Done");
                    if (didworked != IntPtr.Zero)
                    {
                        EnumChildWindows(didworked, EnumChildWindowsCallbackError, IntPtr.Zero);
                        StatusObj.Worked = true;
                        StatusObj.Message = "worked";
                    }

                    IntPtr ditnotwork = FindWindowByCaption(IntPtr.Zero, "BIM Shark - Sync Schedule Error");
                    if (ditnotwork != IntPtr.Zero)
                    {
                        EnumChildWindows(ditnotwork, EnumChildWindowsCallbackError, IntPtr.Zero);
                        StatusObj.Worked = false;
                        StatusObj.Message = "Something went wrong. Try again later";
                    }

                    elapsed += sleep;
                }
            }

            return StatusObj;
        }

        private static void Invoke(ElementSet elements, string strCommandName, string dll)
        {
            //Assembly assembly = Assembly.GetExecutingAssembly();
            //string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            //UriBuilder uri = new UriBuilder(codeBase);
            //string folder = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            //const string folder = "C:\\Users\\verdi\\AppData\\Roaming\\Autodesk\\Revit\\Addins\\2021\\BIMsharkDevelopment\\2021\\";
            //string dll = string.Format("{0}\\{1}", folder, "BIMSharkRevitTools.dll");
            //const string dll = "C:\\Users\\verdi\\AppData\\Roaming\\Autodesk\\Revit\\Addins\\2021\\BIMsharkDevelopment\\2021\\BIMSharkRevitTools.dll";
            UriBuilder dlluri = new UriBuilder(dll);

            string exeConfigPath = Uri.UnescapeDataString(dlluri.Path);

            byte[] assemblyBytes = File.ReadAllBytes(exeConfigPath);

            Assembly objAssembly = Assembly.Load(assemblyBytes);
            IEnumerable<Type> bsType = GetTypesSafely(objAssembly);

            foreach (Type item in bsType)
            {
                Debug.WriteLine(item.Name);
            }
            Type objType = bsType.FirstOrDefault(x => x.Name == strCommandName);

            if (objType.IsClass && string.Equals(objType.Name, strCommandName, StringComparison.OrdinalIgnoreCase))
            {
                object ibaseObject = Activator.CreateInstance(objType);
                object[] arguments = new object[] { null, Autoruntext, elements };
                objType.InvokeMember("Execute", BindingFlags.Default | BindingFlags.InvokeMethod, null, ibaseObject, arguments);
            }
        }

        private static void Invokemethod(ElementSet elements, string strCommandName, string strMethodName, string dll)
        {
            UriBuilder dlluri = new UriBuilder(dll);

            string exeConfigPath = Uri.UnescapeDataString(dlluri.Path);

            byte[] assemblyBytes = File.ReadAllBytes(exeConfigPath);

            Assembly objAssembly = Assembly.Load(assemblyBytes);
            IEnumerable<Type> bsType = GetTypesSafely(objAssembly);

            foreach (Type item in bsType)
            {
                Debug.WriteLine(item.Name);
            }
            Type objType = bsType.FirstOrDefault(x => x.Name == strCommandName);

            if (objType.IsClass && string.Equals(objType.Name, strCommandName, StringComparison.OrdinalIgnoreCase))
            {
                MethodInfo method = objType.GetMethod(strMethodName);

                if (string.Equals(method.Name, strMethodName, StringComparison.OrdinalIgnoreCase))
                {
                    object[] arguments = new object[] { null, Autoruntext, elements };
                    method.Invoke(null, new object[] {});
                    //method.Invoke(null, new object[] { "Hello world " });
                }
            }
        }

        internal static IEnumerable<Type> GetTypesSafely(Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                return ex.Types.Where(x => x != null);
            }
        }

        private static bool EnumChildWindowsCallbackError(IntPtr handle, IntPtr pointer)
        {
            const uint WM_LBUTTONDOWN = 0x0201;
            const uint WM_LBUTTONUP = 0x0202;

            var sb = new StringBuilder(256);

            // Get the control's text.
            GetWindowCaption(handle, sb, 256);
            var text = sb.ToString();

            if (text == "OK")
            {
                PostMessage(handle, WM_LBUTTONDOWN, IntPtr.Zero, IntPtr.Zero);
                PostMessage(handle, WM_LBUTTONUP, IntPtr.Zero, IntPtr.Zero);
                return true;
            }

            return false;
        }
        private static bool EnumChildWindowsCallbackRunSync(IntPtr handle, IntPtr pointer)
        {
            const uint WM_LBUTTONDOWN = 0x0201;
            const uint WM_LBUTTONUP = 0x0202;

            var sb = new StringBuilder(256);

            // Get the control's text.
            GetWindowCaption(handle, sb, 256);
            var text = sb.ToString();

            if (text == "&Ja")
            {
                PostMessage(handle, WM_LBUTTONDOWN, IntPtr.Zero, IntPtr.Zero);
                PostMessage(handle, WM_LBUTTONUP, IntPtr.Zero, IntPtr.Zero);
                return true;
            }
            return false;
        }

        //private static Form GetForm(IntPtr handle)
        //{
        //    return handle == IntPtr.Zero ?
        //        null :
        //        Control.FromHandle(handle) as Form;
        //}

        private delegate void CloseMethod(Form form);

        private static void CloseForm(Form form)
        {
            if (!form.IsDisposed)
            {
                if (form.InvokeRequired)
                {
                    CloseMethod method = new CloseMethod(CloseForm);
                    form.Invoke(method, new object[] { form });
                }
                else
                {
                    form.Close();
                }
            }
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr i);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        private static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "GetWindowText", CharSet = CharSet.Auto)]
        private static extern IntPtr GetWindowCaption(IntPtr hwnd, StringBuilder lpString, int maxCount);

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        // A delegate which is used by EnumChildWindows to execute a callback method.
        private delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);
    }
}
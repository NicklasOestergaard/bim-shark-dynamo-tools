﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.DesignScript.Runtime;

namespace BIMSharkDynamoToolsZeroTouch.Utils
{
    /// <summary>
    /// gh
    /// </summary>
    public sealed class DropDownFunctions
    {
        private DropDownFunctions() { }

        /// <summary>
        /// fghfgh
        /// </summary>
        /// <param name="labels"></param>
        /// <param name="values"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        [IsVisibleInDynamoLibrary(false)]
        public static Dictionary<string, object> GetNodeInput(List<string> labels, List<object> values, int index)
        {
            // TODO - just pass input data unmodified instead?
            var output = new Dictionary<string, object>();
            if (index >= 0)
            {
                output.Add(labels[index], values[index]);
            }

            return output;
        }
    }
}
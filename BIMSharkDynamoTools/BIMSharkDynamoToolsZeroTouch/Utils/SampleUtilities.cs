﻿using Autodesk.DesignScript.Runtime;
using System;

namespace BIMSharkDynamoToolsZeroTouch.Utils
{
    /// <summary>
    /// A utility library containing methods that can be called
    /// from NodeModel nodes, or used as nodes in Dynamo.
    /// </summary>
    public static class SampleUtilities
    {
        /// <summary>
        /// sdfsdf
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [IsVisibleInDynamoLibrary(false)]
        public static double MultiplyInputByNumber(double input)
        {
            return input * 10;
        }

        /// <summary>
        /// fdg
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [IsVisibleInDynamoLibrary(false)]
        public static string DescribeButtonMessage(string input)
        {
            return "Button displays: " + input;
        }

        /// <summary>
        /// fgh
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [IsVisibleInDynamoLibrary(false)]
        public static string DescribeButtonMessageNVO(string input)
        {
            return "NVO! Button displays: " + input;
        }

        /// <summary>
        /// fdg
        /// </summary>
        /// <param name="input1"></param>
        /// <param name="input2"></param>
        /// <param name="input3"></param>
        /// <returns></returns>
        [IsVisibleInDynamoLibrary(false)]
        public static string DescribeButtonMessageNVO2(string input1, string input2, string input3)
        {
            return "NVO! Button displays: " + input1 + input2 + input3;
        }

        /// <summary>
        /// sdfsdf
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [IsVisibleInDynamoLibrary(false)]
        public static bool DescribeMessage(bool input)
        {
            return input;
        }

        /// <summary>
        /// sdfsdf
        /// </summary>
        /// <param name="GUID"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [IsVisibleInDynamoLibrary(false)]
        public static string DescribeWindowMessage(string GUID, string input)
        {
            return "Window displays: Data bridge callback of node " + GUID.Substring(0, 5) + ": " + input;
        }

        /// <summary>
        /// sdfsdf
        /// </summary>
        /// <param name="Run">Set to True to run the node.</param>
        [IsVisibleInDynamoLibrary(false)]
        public static bool BoolParser(object Run)
        {
            if (Run == null)
            {
                throw new ArgumentException("run needs to be a \"Boolean\" value");
            }

            bool run;
            try
            {
                string value = Run.ToString().ToLower();
                if (value == "yes" || value == "true" || value == "1")
                {
                    return run = true;
                }
                else if (value == "no" || value == "false" || value == "0")
                {
                    return run = false;
                }

                run = bool.Parse(value);
            }
            catch (Exception)
            {
                throw new ArgumentException("run needs to be a \"Boolean\" value");
            }

            return run;
        }
    }
}

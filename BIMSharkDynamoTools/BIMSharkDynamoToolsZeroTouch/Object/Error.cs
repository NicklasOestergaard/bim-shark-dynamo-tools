﻿// *****************************************************************************
//
//	Error
//
// *****************************************************************************

using Autodesk.DesignScript.Runtime;

namespace BIMSharkDynamoToolsZeroTouch.Object.BSError
{
    /// <summary>
    /// Parameter related Commands.
    /// </summary>
    //[SupressImportIntoVM]
    [IsVisibleInDynamoLibrary(false)]
    public class BSError
    {
        /// <summary>
        /// Run
        /// </summary>
        public bool Worked { get; set; }

        /// <summary>
        /// message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// ThreadIsActive
        /// </summary>
        public bool ThreadIsActive { get; set; }
    }
}




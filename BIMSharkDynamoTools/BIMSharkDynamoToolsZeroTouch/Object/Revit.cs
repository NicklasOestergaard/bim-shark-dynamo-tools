﻿// *****************************************************************************
//
//	Command
//
// *****************************************************************************

using Autodesk.DesignScript.Runtime;

namespace BIMSharkDynamoToolsZeroTouch.Object.Revit
{
    /// <summary>
    /// Parameter related Commands.
    /// </summary>
    //[SupressImportIntoVM]
    [IsVisibleInDynamoLibrary(false)]
    public class Command
    {
        /// <summary>
        /// Command key
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Command key
        /// </summary>
        public string CommandId { get; set; }
    }
}

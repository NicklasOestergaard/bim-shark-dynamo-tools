;// Defining variables //
#define Repository     "..\."
#define MyAppName      "BIMSharkDynamoTools"
#define MyAppVersion GetFileVersion("..\dynamo_packages\BIMSharkDynamoTools\bin\BIMSharkDynamoToolsZeroTouch.dll")
#define MyAppProductVersion GetStringFileInfo("..\dynamo_packages\BIMSharkDynamoTools\bin\BIMSharkDynamoToolsZeroTouch.dll", "ProductVersion")
#define MyAppPublisher "BIM Shark ApS"
#define MyAppURL       "https://www.bimshark.com/"
;// MyAppExeName //
#define MyAppExeName   "BIMSharkDynamoTools2.50_" + MyAppVersion
#define RevitAppName  "BIMSharkDynamoTools"

[Setup]
PrivilegesRequired=lowest
CloseApplications=force
PrivilegesRequiredOverridesAllowed=commandline dialog
SignTool=signtoolev
AppId={{E6FF585A-CB65-4D9F-B10C-86F566FC85F0}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={autoappdata}\Dynamo\Dynamo Revit\2.5\packages\{#MyAppName}
UsePreviousAppDir=no
DisableDirPage=no
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
DisableWelcomePage=no
OutputDir={#Repository}
OutputBaseFilename={#MyAppExeName}
Compression=lzma
SolidCompression=yes
WizardSmallImageFile=icon138x140.bmp
WizardImageFile=logo410x797.bmp
ChangesAssociations=no
WizardStyle=modern
EnableDirDoesntExistWarning=True
UninstallDisplayIcon={uninstallexe}

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Types]
; Name: "custom"; Description: "Custom installation"; Flags: iscustom

[Components]
; Name: "Dynamo250"; Description: "BIM Shark Dynamo Tools for Dynamo 2.5.0"; Types: custom

[Dirs]
Name: "{app}"; Permissions: everyone-full
Name: "{app}\bin"
Name: "{app}\bin\en-US"
Name: "{app}\bin\Resources"
Name: "{app}\dyf"
Name: "{app}\extra"
Name: "{app}\extra\player"

[Files]
Source: "..\dynamo_packages\BIMSharkDynamoTools\pkg.json"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\bin\BIMSharkDynamoToolsUI.dll"; DestDir: "{app}\bin"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\bin\BIMSharkDynamoToolsUI.dll.config"; DestDir: "{app}\bin"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\bin\BIMSharkDynamoToolsUI.XML"; DestDir: "{app}\bin"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\bin\BIMSharkDynamoToolsZeroTouch.dll"; DestDir: "{app}\bin"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\bin\BIMSharkDynamoToolsZeroTouch.XML"; DestDir: "{app}\bin"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\bin\BIMSharkDynamoToolsZeroTouch_DynamoCustomization.xml"; DestDir: "{app}\bin"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\bin\en-US\BIMSharkDynamoToolsUI.resources.dll"; DestDir: "{app}\bin\en-US"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\bin\en-US\BIMSharkDynamoToolsUI.XML"; DestDir: "{app}\bin\en-US"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\dyf\ColorElementsByTypeParameter.dyf"; DestDir: "{app}\dyf"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\dyf\DocumentFilePath.dyf"; DestDir: "{app}\dyf"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\dyf\ElementsInBoudingBox.dyf"; DestDir: "{app}\dyf"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\dyf\GetCommandIdByName.dyf"; DestDir: "{app}\dyf"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\dyf\GetTypeParameterFormElements.dyf"; DestDir: "{app}\dyf"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\dyf\JSONArrayToList.dyf"; DestDir: "{app}\dyf"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\dyf\PostCommandById.dyf"; DestDir: "{app}\dyf"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\dyf\RESTCallBase.dyf"; DestDir: "{app}\dyf"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\dyf\SetWorksetBasedOnScopebox.dyf"; DestDir: "{app}\dyf"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - Color Elements By Type Parameter.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - Delete All BIM Shark Parameters.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - Dynamo Sample.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - Dynamo Sample2.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - Export schedule.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - External Command Launch BIMShark SyncNow.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - External Command Launch BIMShark SyncNow2.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - External Command Launch BIMShark SyncNow3.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - Set BSOverride.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - Set ProjectId.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - Sheetset By Revison Date.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - SyncNow.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion
Source: "..\dynamo_packages\BIMSharkDynamoTools\extra\BIM Shark - SyncSchedule.dyn"; DestDir: "{app}\extra"; Flags: ignoreversion

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:UninstallProgram, {#MyAppName}}"; Filename: "{uninstallexe}"

[Registry]

[UninstallDelete]

[Code]

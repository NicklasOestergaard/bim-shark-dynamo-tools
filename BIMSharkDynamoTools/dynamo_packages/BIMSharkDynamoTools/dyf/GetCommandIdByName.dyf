{
  "Uuid": "21aa7368-13b6-4985-8c3e-2d37e0730ba8",
  "IsCustomNode": true,
  "Category": "BIMSharkDynamoTools.Revit",
  "Description": "Get Command Id By Name",
  "Name": "GetCommandIdByName",
  "ElementResolver": {
    "ResolutionMap": {}
  },
  "Inputs": [],
  "Outputs": [],
  "Nodes": [
    {
      "ConcreteType": "Dynamo.Graph.Nodes.ZeroTouch.DSFunction, DynamoCore",
      "NodeType": "FunctionNode",
      "FunctionSignature": "DSCore.List.IndexOf@var[]..[],var",
      "Id": "fc216e807731415a91b6e479236277b8",
      "Inputs": [
        {
          "Id": "74735571df574f8aab1d950367b975ac",
          "Name": "list",
          "Description": "The list to find the element in.\n\nvar[]..[]",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        },
        {
          "Id": "d7fe9b8e70cc4650aaba66bf89c9d7ab",
          "Name": "element",
          "Description": "The element whose index is to be returned.\n\nvar",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        }
      ],
      "Outputs": [
        {
          "Id": "31e35011814549369d30561b193f4a6a",
          "Name": "int",
          "Description": "The index of the element in the list.",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        }
      ],
      "Replication": "Auto",
      "Description": "Returns the index of the element in the given list.\n\nList.IndexOf (list: var[]..[], element: var): int"
    },
    {
      "ConcreteType": "Dynamo.Graph.Nodes.ZeroTouch.DSFunction, DynamoCore",
      "NodeType": "FunctionNode",
      "FunctionSignature": "DSCore.List.GetItemAtIndex@var[]..[],int",
      "Id": "88c8c7bf7a0f467595815b091f675df8",
      "Inputs": [
        {
          "Id": "ccbadaab164e4122a50a269557752d60",
          "Name": "list",
          "Description": "List to fetch an item from.\n\nvar[]..[]",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        },
        {
          "Id": "7400f43459ae4440acc4554dec7f4880",
          "Name": "index",
          "Description": "Index of the item to be fetched.\n\nint",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        }
      ],
      "Outputs": [
        {
          "Id": "4250241518834c5099cb5adb3cc7f032",
          "Name": "item",
          "Description": "Item in the list at the given index.",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        }
      ],
      "Replication": "Auto",
      "Description": "Returns an item from the given list that's located at the specified index.\n\nList.GetItemAtIndex (list: var[]..[], index: int): var[]..[]"
    },
    {
      "ConcreteType": "Dynamo.Graph.Nodes.ZeroTouch.DSFunction, DynamoCore",
      "NodeType": "FunctionNode",
      "FunctionSignature": "BIMSharkDynamoToolsZeroTouch.Revit.Revit.GetCommandKey@var[]",
      "Id": "30975f9bebb848889fca80e29b8d2035",
      "Inputs": [
        {
          "Id": "0dfc8e209ac04aaa9c4cb25a3e1635f7",
          "Name": "Command",
          "Description": "var[]",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        }
      ],
      "Outputs": [
        {
          "Id": "41f74e98617041df9a6bc6640d0266d4",
          "Name": "key",
          "Description": "ALL BIM Shark Parameters.",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        }
      ],
      "Replication": "Auto",
      "Description": "Returns all BIM Shark Parameters found in document.\n\nRevit.GetCommandKey (Command: var[]): string[]"
    },
    {
      "ConcreteType": "Dynamo.Graph.Nodes.ZeroTouch.DSFunction, DynamoCore",
      "NodeType": "FunctionNode",
      "FunctionSignature": "BIMSharkDynamoToolsZeroTouch.Revit.Revit.GetAllBSCommands",
      "Id": "7eb7b34c5f724a4dabd0ce981bc3504b",
      "Inputs": [],
      "Outputs": [
        {
          "Id": "133ee6871ea641e69625f4ade080e417",
          "Name": "var[]",
          "Description": "var[]",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        }
      ],
      "Replication": "Auto",
      "Description": "Returns all BIM Shark Commands\n\nRevit.GetAllBSCommands ( ): var[]"
    },
    {
      "ConcreteType": "Dynamo.Graph.Nodes.CustomNodes.Symbol, DynamoCore",
      "NodeType": "InputNode",
      "Parameter": {
        "Name": "name",
        "TypeName": "var",
        "TypeRank": 0,
        "DefaultValue": null,
        "Description": ""
      },
      "Id": "5e72369837d44c1d98ec034810e87a03",
      "Inputs": [],
      "Outputs": [
        {
          "Id": "53a72b9c33eb40f5b54fa98855fbc3e3",
          "Name": "",
          "Description": "Symbol",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        }
      ],
      "Replication": "Disabled",
      "Description": "A function parameter, use with custom nodes.\r\n\r\nYou can specify the type and default value for parameter. E.g.,\r\n\r\ninput : var[]..[]\r\nvalue : bool = false"
    },
    {
      "ConcreteType": "Dynamo.Graph.Nodes.CustomNodes.Output, DynamoCore",
      "NodeType": "OutputNode",
      "ElementResolver": null,
      "Symbol": "command",
      "Id": "cd2a446c4ccc48ebadc54e2624f58113",
      "Inputs": [
        {
          "Id": "904d57f0527f4dccbb35464b3b0f8d09",
          "Name": "",
          "Description": "",
          "UsingDefaultValue": false,
          "Level": 2,
          "UseLevels": false,
          "KeepListStructure": false
        }
      ],
      "Outputs": [],
      "Replication": "Disabled",
      "Description": "A function output, use with custom nodes"
    }
  ],
  "Connectors": [
    {
      "Start": "31e35011814549369d30561b193f4a6a",
      "End": "7400f43459ae4440acc4554dec7f4880",
      "Id": "32bba758bcb946638a55fad02700441a"
    },
    {
      "Start": "4250241518834c5099cb5adb3cc7f032",
      "End": "904d57f0527f4dccbb35464b3b0f8d09",
      "Id": "fb766909e5554259ae6185a5aa5f0caf"
    },
    {
      "Start": "41f74e98617041df9a6bc6640d0266d4",
      "End": "74735571df574f8aab1d950367b975ac",
      "Id": "438da52caf8d42d58650ebbea62c780c"
    },
    {
      "Start": "133ee6871ea641e69625f4ade080e417",
      "End": "0dfc8e209ac04aaa9c4cb25a3e1635f7",
      "Id": "89b4ad3e991c4511a714f6fb56c33d37"
    },
    {
      "Start": "133ee6871ea641e69625f4ade080e417",
      "End": "ccbadaab164e4122a50a269557752d60",
      "Id": "fbf55dc4324648148540479d13bacbdd"
    },
    {
      "Start": "53a72b9c33eb40f5b54fa98855fbc3e3",
      "End": "d7fe9b8e70cc4650aaba66bf89c9d7ab",
      "Id": "fe2293d3d2f742a6985a9e1ba004a18f"
    }
  ],
  "Dependencies": [],
  "Bindings": [],
  "View": {
    "Dynamo": {
      "ScaleFactor": 1.0,
      "HasRunWithoutCrash": false,
      "IsVisibleInDynamoLibrary": true,
      "Version": "2.1.0.7500",
      "RunType": "Manual",
      "RunPeriod": "1000"
    },
    "Camera": {
      "Name": "Background Preview",
      "EyeX": -17.0,
      "EyeY": 24.0,
      "EyeZ": 50.0,
      "LookX": 12.0,
      "LookY": -13.0,
      "LookZ": -58.0,
      "UpX": 0.0,
      "UpY": 1.0,
      "UpZ": 0.0
    },
    "NodeViews": [
      {
        "ShowGeometry": true,
        "Name": "List.IndexOf",
        "Id": "fc216e807731415a91b6e479236277b8",
        "IsSetAsInput": false,
        "IsSetAsOutput": false,
        "Excluded": false,
        "X": 974.0744561248614,
        "Y": 16.078892235333569
      },
      {
        "ShowGeometry": true,
        "Name": "List.GetItemAtIndex",
        "Id": "88c8c7bf7a0f467595815b091f675df8",
        "IsSetAsInput": false,
        "IsSetAsOutput": false,
        "Excluded": false,
        "X": 1255.1389625714703,
        "Y": 0.0
      },
      {
        "ShowGeometry": true,
        "Name": "Revit.GetCommandKey",
        "Id": "30975f9bebb848889fca80e29b8d2035",
        "IsSetAsInput": false,
        "IsSetAsOutput": false,
        "Excluded": false,
        "X": 548.20691615842566,
        "Y": -28.329937552840619
      },
      {
        "ShowGeometry": true,
        "Name": "Revit.GetAllBSCommands",
        "Id": "7eb7b34c5f724a4dabd0ce981bc3504b",
        "IsSetAsInput": false,
        "IsSetAsOutput": false,
        "Excluded": false,
        "X": 35.677163000322821,
        "Y": 91.747896124342532
      },
      {
        "ShowGeometry": true,
        "Name": "Input",
        "Id": "5e72369837d44c1d98ec034810e87a03",
        "IsSetAsInput": false,
        "IsSetAsOutput": false,
        "Excluded": false,
        "X": 0.0,
        "Y": 0.0
      },
      {
        "ShowGeometry": true,
        "Name": "Output",
        "Id": "cd2a446c4ccc48ebadc54e2624f58113",
        "IsSetAsInput": false,
        "IsSetAsOutput": false,
        "Excluded": false,
        "X": 1509.1389625714703,
        "Y": 0.0
      }
    ],
    "Annotations": [],
    "X": -33.962375286339864,
    "Y": 205.19067204096564,
    "Zoom": 0.68320669405256829
  }
}